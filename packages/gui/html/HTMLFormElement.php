<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */

//namespace gui\html;

import('gui.HTMLElement');

/**
 * Base class for html form elements.
 *
 * @package gui.html
 * @author Demián Andrés Rodriguez (demian85@gmail.com)
 */
abstract class HTMLFormElement extends HTMLElement {

	protected function __construct($name = null, $value = '') {
		parent::__construct();
		$this->setName($name);
		$this->setValue($value);
	}

	public function setName($name) {
		$this->setAttr('name', $name);
		return $this;
	}

	public function setValue($value) {
		$this->setAttr('value', $value);
		return $this;
	}
}
?>