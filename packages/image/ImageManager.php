<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */

// namespace image;

/**
 * This class has utility methods for image manipulation.
 *
 * @package image
 * @author ZedPlan Team (opencorephp@zedplan.com)
 */
class ImageManager {
	/**
	 * @var string
	 */
	protected $imagePath;

	/**
	 * Create an instance of this class.
	 *
	 * @param string $imagePath Image file path.
	 * @throws FileNotFoundException if $imagePath is invalid.
	 */
	public function __construct($imagePath)
	{
		if (!file_exists($imagePath)) {
			import('io.FileNotFoundException');
			throw new FileNotFoundException("\"$imagePath\" is not a valid font source file.");
		}

		$this->imagePath = rtrim($imagePath, "\\/");
	}

	/**
	 * Resize image keeping aspect ratio. Does not upscale image for best fit.
	 * This method fixes native Imagick#thumbnailImage because it sucks.
	 *
	 * @param int $width
	 * @param int $height
	 * @param string $targetFile Target file name. If null the source image will be replaced.
	 * @return void
	 */
	public function resize($width, $height, $targetFile = null)
	{
		if (!class_exists('Imagick')) {
			throw new RuntimeException("Imagick class not found!");
		}

		$imagick = new Imagick($this->imagePath);
		$ratio = $imagick->getImageWidth() / $imagick->getImageHeight();
		$w = $ratio >= 1 && $width < $imagick->getImageWidth() ? $width : 0;
		$h = $ratio < 1 && $height < $imagick->getImageHeight() ? $height : 0;
		if ($w || $h) $imagick->thumbnailImage($w, $h, false);
		$imagick->writeImage($targetFile ? $targetFile : $this->imagePath);
	}
}
?>
